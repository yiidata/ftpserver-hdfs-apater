package com.yiidata.ftpserver;

import com.yiidata.ftpserver.hdfs.HdfsFileSystemFactory;
import com.yiidata.ftpserver.hdfs.HdfsFileSystemView;
import org.apache.ftpserver.ftplet.FileSystemView;
import org.apache.ftpserver.ftplet.FtpFile;
import org.apache.ftpserver.ftplet.User;
import org.apache.ftpserver.ftplet.UserManager;
import org.apache.ftpserver.usermanager.PropertiesUserManagerFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.permission.FsPermission;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * <pre>
 *
 * Created by IntelliJ IDEA.
 * User: zhenqin
 * Date: 15/10/29
 * Time: 13:53
 * To change this template use File | Settings | File Templates.
 *
 * </pre>
 *
 * @author zhenqin
 */
public class HdfsFtpFileTest {


    private final Logger log = LoggerFactory.getLogger(HdfsFtpFileTest.class);

    @Test
    public void testFile() throws Exception {
        PropertiesUserManagerFactory userManagerFactory = new PropertiesUserManagerFactory();
        userManagerFactory.setFile(new File("src/main/resources/users.properties"));

        UserManager userManager = userManagerFactory.createUserManager();
        User admin = userManager.getUserByName("admin");

        HdfsFileSystemFactory fileSystemFactory = new HdfsFileSystemFactory(new Configuration());

        FileSystemView fileSystemView = fileSystemFactory.createFileSystemView(admin);

        FtpFile homeDirectory = fileSystemView.getHomeDirectory();
        System.out.println(homeDirectory.getName());

        FtpFile workingDirectory = fileSystemView.getWorkingDirectory();
        System.out.println(workingDirectory.getName());

        boolean res = fileSystemView.changeWorkingDirectory("res");
        System.out.println(res);
    }


    @Test
    public void testHDFSFile() throws Exception {
        PropertiesUserManagerFactory userManagerFactory = new PropertiesUserManagerFactory();
        userManagerFactory.setFile(new File("src/main/resources/users.properties"));

        UserManager userManager = userManagerFactory.createUserManager();
        User admin = userManager.getUserByName("admin");

        HdfsFileSystemFactory fileSystemFactory = new HdfsFileSystemFactory(new Configuration());

        FileSystemView fileSystemView = fileSystemFactory.createFileSystemView(admin);

        FtpFile homeDirectory = fileSystemView.getHomeDirectory();

        final FtpFile ftpFile = fileSystemView.getFile("/data");

        log.info(homeDirectory.getName());
        log.info(homeDirectory.getOwnerName());
        log.info(homeDirectory.getGroupName());
        log.info(homeDirectory.getAbsolutePath());


        log.info("isReadable {}", homeDirectory.isReadable());
        log.info("isDirectory {}", homeDirectory.isDirectory());
        log.info("isWritable {}", homeDirectory.isWritable());
        log.info("isRemovable {}", homeDirectory.isRemovable());
    }


    @Test
    public void testHDFSNotUserFile() throws Exception {
        PropertiesUserManagerFactory userManagerFactory = new PropertiesUserManagerFactory();
        userManagerFactory.setFile(new File("src/main/resources/users.properties"));

        UserManager userManager = userManagerFactory.createUserManager();
        User admin = userManager.getUserByName("admin");

        HdfsFileSystemFactory fileSystemFactory = new HdfsFileSystemFactory(new Configuration());

        FileSystemView fileSystemView = fileSystemFactory.createFileSystemView(admin);
        final FtpFile ftpFile = fileSystemView.getFile("/data/hive");

        log.info(ftpFile.getName());
        log.info(ftpFile.getOwnerName());
        log.info(ftpFile.getGroupName());
        log.info(ftpFile.getAbsolutePath());


        log.info("isReadable {}", ftpFile.isReadable());
        log.info("isDirectory {}", ftpFile.isDirectory());
        log.info("isWritable {}", ftpFile.isWritable());
        log.info("isRemovable {}", ftpFile.isRemovable());
    }


    @Test
    public void testStartFtpServer() throws Exception {
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(conf);
        FsPermission permission = fs.getFileLinkStatus(new Path("/user/hadoop")).getPermission();
        System.out.println(permission.getUserAction().SYMBOL);
        System.out.println(permission.getGroupAction().SYMBOL);
        System.out.println(permission.getOtherAction().SYMBOL);
    }



    @Test
    public void testHDFSPath () throws Exception {
        Path f = new Path("/hello/world/for");
        System.out.println(f.getParent().toString());
    }
}
