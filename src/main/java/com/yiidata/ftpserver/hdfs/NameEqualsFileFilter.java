package com.yiidata.ftpserver.hdfs;

import java.io.File;
import java.io.FileFilter;

/**
 * <pre>
 *
 * Created by zhenqin.
 * User: zhenqin
 * Date: 2018/7/12
 * Time: 13:47
 * Verdor: NowledgeData
 * To change this template use File | Settings | File Templates.
 *
 * </pre>
 *
 * @author zhenqin
 */
public class NameEqualsFileFilter implements FileFilter {

    private String nameToMatch;

    private boolean caseInsensitive = false;

    /**
     * Constructor
     *
     * @param nameToMatch
     *            The exact file name to match
     * @param caseInsensitive
     *            Wether that match should be case insensitive
     */
    public NameEqualsFileFilter(final String nameToMatch,
                                final boolean caseInsensitive) {
        this.nameToMatch = nameToMatch;
        this.caseInsensitive = caseInsensitive;
    }

    public boolean accept(final File file) {

        if (caseInsensitive) {
            return file.getName().equalsIgnoreCase(nameToMatch);
        } else {
            return file.getName().equals(nameToMatch);
        }
    }

}
