#Apache FtpServer Hadoop HDFS Adapter

---
##项目说明

本项目是一个让 Apache FTpServer(http://mina.apache.org/downloads-ftpserver.html) 支持 HDFS 文件系统的组件. 全代码只要三个类, 但是也是本人走查 ftpserver 代码的心血. 当看过这个项目, 测试本地 FTP 成功后, 灵光闪现, 要是能支持 Hadoop 多好, 因此走读代码, 发现编写也很简单, 因此有了该组件.

希望对广大 hadoop 爱好者, 有相关需求的朋友有用.

##配置方式:

1. 把 Hadoop 相关 jar 放到 ${apache-ftpserver}/common/lib 下
2. 把本项目使用 maven 打包, 放置到${apache-ftpserver}/common/lib 下
3. 把构建 Hadoop FileSystem 的 configuration 相关的 core-site.xml 和 hdfs-site.xml 放到${apache-ftpserver}/common/classes 下
4. Copy ftpd-typical.xml 为 ftpd-hdfs.xml, 并修改其中配置如下:

'''

	<listeners>
		<nio-listener name="default" port="2121" local-address="0.0.0.0"  implicit-ssl="false">
        </nio-listener>
	</listeners>
    <file-user-manager file="./res/conf/users.properties" />

    <filesystem>
		<beans:bean id="hdfsFileSystem" class="com.yiidata.ftpserver.hdfs.HdfsFileSystemFactory">
            <beans:constructor-arg type="org.apache.hadoop.conf.Configuration">
                <beans:bean class="org.apache.hadoop.conf.Configuration" />
            </beans:constructor-arg>
        </beans:bean>
	</filesystem>

'''

5. 编辑 users.properties 文件, 修改 admin 的 home为 /user/youname, 同时修改anonymous
6. 注意 ftpd-hdfs.xml 顶部的命名空间, 增加:

'''

    <server xmlns="http://mina.apache.org/ftpserver/spring/v1"
	    xmlns:beans="http://www.springframework.org/schema/beans" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	    xsi:schemaLocation="
	   http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.5.xsd 
	   http://mina.apache.org/ftpserver/spring/v1 http://mina.apache.org/ftpserver/ftpserver-1.0.xsd	
	   "
	id="myServer">
	
'''

7. 配置完成, 执行 ./bin/ftpd.sh start res/conf/ftpd-hdfs.xml 完成启动. 
8. HDFS 文件系统是不支持随机写的, 所以不支持断点续传(下载支持断点续传, 上传不支持). 其它功能基本可用. 
9. 亲测可用, 祝君顺利.


项目名: adapter, 拼写错误, 错了就错了吧. 不要介意